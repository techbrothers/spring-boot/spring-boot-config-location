package com.techbrothers.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.techbrothers.config.ApplicationProperties;

@Controller
public class DisplayController {
	
	@Autowired
	ApplicationProperties applicationProperties;
	
	@GetMapping("/data")
	public String getData(Model model) {
		model.addAttribute("firstName", applicationProperties.getFirstName());
		model.addAttribute("lastName", applicationProperties.getLastName());
		model.addAttribute("age", applicationProperties.getAge());
		return "data";
	}

}
